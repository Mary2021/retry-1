const user = require('./user');

describe('userID testing', () => {

    test('user id=1 data', () => {
        const userData = user(1);
        expect(userData).toMatchSnapshot();
    });

    test('user id=56 data', () => {
        const userData = user(56);
        expect(userData).toMatchSnapshot();
    });

    test('user id=1345 data', () => {
        const userData = user(1345);
        expect(userData).toMatchSnapshot();
    });
});