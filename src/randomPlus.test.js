const random = require('./random');
const randomPlus = require('./randomPlus');
jest.mock('./random');

describe ('test randomPlus', () => {
    test('by adding 5 to random 1 you get 6', () => {
        
        random.mockImplementation (() => 1);
        const value = randomPlus(5);
        expect(value).toBe(6);
    });

    test('by adding 1 to random 99 you get 100', () => {
        
        random.mockImplementation (() => 99);
        const value = randomPlus(1);
        expect(value).toBe(100);
    });
});

module.exports = function randomPlus(number) {
    return randomPlus(1, 99) + number;
};