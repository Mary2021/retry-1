const userID = require('../src/user');

describe('user', () =>{
    it('exception when string given', () => {
        expect(() => {
            userID('aaa111');
        }).toThrow();
    });
});